# Bash like Linux Shell in C

## Introduction

This is an implementation of a Linux shell written in C language.


## Run the shell

1. Clone this directory and `cd` into it.
2. Run the command `makefile`, this results in creation of an executable file `myshell`.
3. Run `./myshell` to get a prompt of the form `username@system_name:path`.
4. In order to exit, run `quit`.

## Features

### Built-in commands

These commands have been defined by me and are contained within the shell itself.

1. `pwd`
    
    - Displays the name of the working directory.
    - Implemented in [pwd.c](pwd.c)

2. `ls [-l -a] [directory]`
    
    - Lists all the files and directories in the specified directory.
    - Variations such as `ls, ls . , ls ..` will also work.
    - Also handles multiple directories as arguments. eg. `ls -l dir1 dir2 dir3`.
    - Throws error if you try to `ls` on anything except a valid directory. 
    - Implemented in [ls.c](ls.c)

3. `cd [file]`
    
    - Changes directory to the directory specified, throws an error if the directory does not exist.
    - Implemented in [cd.c](cd.c)

4. `echo [arguments]`
    
    - Displays whatever is specified in [arguments]. 
    - Accounts for double quotes as well.
    - Implemented in [echo.c](echo.c)

5. `quit`

    - Exits the shell.

6. `setenv var[value]` & `unset var`

    - Creates an enviornmental variable `var` if it doesn't already exist and assigns it the value given
    - `unset var` destroys that environmental variable

7.  `jobs`

    - Prints a list of all currently running jobs along with their pid in order of their creation
    - Gives the state of the job – Running, Sleeping, Stopped or Defunct

8. `kjob <jobNumber> <signalNumber>` 
    
    - Takes the job id of a running job and sends a signal value to that process

9. `fg <jobNumber>`
    
    - Brings a running or a stopped background job of given job number to foreground.

10. `bg <jobNumber>`

    - Changes a stopped background job to a running background job.

11. `pinfo [PID]`

    - Prints numerous details about the process such as its status, memory, and executable path.
    - Just `pinfo` with no arguments gives details of the shell.
    - Implemented in [pinfo.c](pinfo.c)

12. `overkill`

    - ​ Kills all background process at once.

### Foreground and Background Processes

- All other commands are treated as system commands like vim, ping etc.
- To run a process in the background, follow the command with a '&' symbol. Eg. `vim &`.
- Upon termination of a background process, the shell prints its PID and exit status.
- Handles `&` no matter where it is in the end. eg. `emacs& , emacs         &`.



## Additional Features

1. `​CTRL-Z`

    - Changes the status of currently running job to stop, and pushes it to the background.

2. `CTRL-C`

    - Sends SIGINT signal to the current foreground job of the shell​.
    - If there is no foreground job, then the signal does not have any effect.

3. Input-Output Redirection & Piping

    - Handles `<`, `>`, `>>`, and `|` operators appropriately, wherever they are in the command.
    - Throws error if syntax is incorrect.
4. `;` seperated commands

    - Handles semi-colon sperated commands.
5. Also handles commands that has combinaton of redirection, piping and semi-colon.
