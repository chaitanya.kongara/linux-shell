#include "headers.h"
#include "beautify.h"

char* beautify(char *dup){
    char*input=(char*)calloc(1024,sizeof(char));
    int i=0,dup_siz=0;
    for(i=0;dup[i]!='\0';i++) dup_siz++;
    for(i=0;i<(dup_siz);i++) if(dup[i]!=' ') break;
    int start=i; 
    for(i=dup_siz-1;i>=0;i--) if(dup[i]!=' ') break;
    int end=i;
    int inp_siz=0;
    int flag=0;
    for(i=start;i<=end;i++){
        if(dup[i]!=' ') {
           if(flag){
               input[inp_siz++]=' ';
               input[inp_siz++]=dup[i];
           }
           else input[inp_siz++]=dup[i];
           flag=0;
        }
        else flag=1;
    }
    input[inp_siz]='\0';
    return input;
}