#include "headers.h"
#include "env.h"


void setenvi(char * input,int *bonus2){
    int i=7;
    char var[1024];
    char val[1024];
    int k=0;
    for(;input[i]!=' '&&input[i]!='\0';i++){
        var[k++]=input[i];
    }
    var[k]='\0';
    k=0;
    if(input[i]!='\0') i++;
    for(;input[i]!=' '&&input[i]!='\0';i++){
        val[k++]=input[i];
    }
    val[k]='\0';
    if(input[i]=='\0') setenv(var,val,1);
    else {printf("ERROR : no.of arguments > 2.\n");(*bonus2)*=0;}
}

void unsetenvi(char *input,int*bonus2){
    int i=9;
    if(input[9]=='\0') {
        printf("ERROR : should give an argument.\n");
        (*bonus2)*=0;
        return ;    
    }
    char var[1024];
    int k=0;
    for(;input[i]!=' '&&input[i]!='\0';i++){
        var[k++]=input[i];
    }
    var[k]='\0';
    if(input[i]=='\0') unsetenv(var);
    else {printf("ERROR : given multiple arguments.\n");(*bonus2)*=0;}

}