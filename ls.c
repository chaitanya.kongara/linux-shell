#include "headers.h"
#include "ls.h"
#include "pwd.h"
void ls(char *dir,char * tilde,int * bonus2){
    char name[1024],start[1024];
    //printf("%s\n",dir);
    int errflag=0;
    if(!getcwd(start,1024)) perror("error while using getcwd in ls.c");
    int dir_siz=0;
    for(int i=0;dir[i]!='\0';i++) dir_siz++;
    if(dir_siz==1&&dir[0]=='~') {if(chdir(tilde)<0){ perror("error while using chdir in ls.c.");(*bonus2)*=0;}else errflag=1;}
    else if(dir_siz>=2&&dir[0]=='~'&&dir[1]=='/') {
        char ex[1024];
        int k=0,x=0;
        for(int i=0;tilde[i]!='\0';i++) ex[k++]=tilde[i];
        for(int i=1;i<dir_siz;i++) ex[k++]=dir[i];
        for(int i=0;i<k;i++) dir[x++]=ex[i];
        dir[x]='\0';
        //printf("%s\n",dir);
        if(chdir(dir)<0) {perror("error while using chdir in ls.c");(*bonus2)*=0;}
        else errflag=1;
    }  
    else {if(chdir(dir)<0) {perror("error while using chdir in ls.c");(*bonus2)*=0;}else errflag=1;}
    if(errflag==1){
    if(!getcwd(name,1024)) perror("error while using getcwd in ls.c");
    struct dirent *D;
    DIR *dr = opendir(name);
    if(dr== NULL) perror("error while using opendir in ls.c");
    else{
    while(1){
        D=readdir(dr);
        if(D==NULL&&errno>0&&errno<14) {perror("error while using readdir in ls.c");(*bonus2)*=0;}
        else if(D==NULL) break;
        else  {
            if(D->d_name[0]!='.') printf("%s\n",D->d_name);
        }
        
    }
    closedir(dr);}
    chdir(start);}
}

void ls_a(char*dir,char * tilde,int *bonus2){
    char name[1024],start[1024];
    int errflag=0;
    //printf("%s\n",dir);
    if(!getcwd(start,1024)) perror("error while using getcwd in ls.c");
    int dir_siz=0;
    for(int i=0;dir[i]!='\0';i++) dir_siz++;
    if(dir_siz==1&&dir[0]=='~') {if(chdir(tilde)<0) {perror("error while using chdir in ls.c.");(*bonus2)*=0;}else errflag=1;}
    else if(dir_siz>=2&&dir[0]=='~'&&dir[1]=='/') {
        char ex[1024];
        int k=0,x=0;
        for(int i=0;tilde[i]!='\0';i++) ex[k++]=tilde[i];
        for(int i=1;i<dir_siz;i++) ex[k++]=dir[i];
        for(int i=0;i<k;i++) dir[x++]=ex[i];
        dir[x]='\0';
        //printf("%s\n",dir);
        if(chdir(dir)<0) perror("error while using chdir in ls.c");
        else errflag=1;
       // pwd();
    } 
    else {if(chdir(dir)<0) {perror("error while using chdir in ls.c");(*bonus2)*=0;}else errflag=1;}
    if(errflag==1){
    if(!getcwd(name,1024)) perror("error while using getcwd in ls.c");
    printf("%s",name);
    struct dirent *D;
    DIR *dr = opendir(name);
    if(dr== NULL) perror("error while using opendir in ls.c");
    else{
    while(1){
        D=readdir(dr);
        //printf("%d ",errno);
        if(D==NULL&&errno>0&&errno<14){ perror("error while using readdir in ls.c");(*bonus2)*=0;}
        else if(D==NULL) break;
        else  {
            printf("%s\n",D->d_name);
        }
        
    }
    closedir(dr);}
    chdir(start);}
}

void ls_l(char*dir,char * tilde,int *bonus2){
     char namef[1024];
     char name[1024];
    getcwd(namef,1024);
    int dir_siz=0,errflag=0;
    for(int i=0;dir[i]!='\0';i++) dir_siz++;
    if(dir_siz==1&&dir[0]=='~') {if(chdir(tilde)<0) perror("error while using chdir in ls.c");
    else errflag=1;}
    else if(dir_siz>=2&&dir[0]=='~'&&dir[1]=='/') {
        char ex[1024];
        int k=0,x=0;
        for(int i=0;tilde[i]!='\0';i++) ex[k++]=tilde[i];
        for(int i=1;i<dir_siz;i++) ex[k++]=dir[i];
        for(int i=0;i<k;i++) dir[x++]=ex[i];
        dir[x]='\0';
        //printf("%s\n",dir);
        if(chdir(dir)<0) {perror("error while using chdir in ls.c");(*bonus2)*=0;}
        else errflag=1;
       //pwd();
    } 
    else {if(chdir(dir)<0){ perror("error while using chdir in ls.c");(*bonus2)*=0;}
    else errflag=1;}
    if(errflag==1){
    getcwd(name,1024);
    struct dirent *D;
    struct stat info;
    struct passwd *u;
    struct group *g;
    int total=0;
    DIR *dr = opendir(name);
    if(dr== NULL) perror("error while using opendir");
    while((D=readdir(dr))!=NULL) {
        if(stat(D->d_name,&info)<0) perror("error while using stat");
        else{
            if(D->d_name[0]!='.'){
            printf((info.st_mode & S_IFDIR)?"d":"-");  
            printf((info.st_mode & S_IRUSR)?"r":"-");
            printf((info.st_mode & S_IWUSR)?"w":"-");
            printf((info.st_mode & S_IXUSR)?"x":"-");
            printf((info.st_mode & S_IRGRP)?"r":"-");
            printf((info.st_mode & S_IWGRP)?"w":"-");
            printf((info.st_mode & S_IXGRP)?"x":"-");
            printf((info.st_mode & S_IROTH)?"r":"-");
            printf((info.st_mode & S_IWOTH)?"w":"-");
            printf((info.st_mode & S_IXOTH)?"x":"-");
            // struct stat buf;
            // stat(".",&buf);
            // printf("total: %ld ",buf.st_blksize);
            // total+=buf.st_blksize;
            printf(" %10lu",info.st_nlink);
            u=getpwuid(info.st_uid);
            g=getgrgid(info.st_gid);
            printf(" %20s %20s",u->pw_name,g->gr_name);
            char str[100];
            sprintf(str," %15ld",info.st_size);
            int cnt=0;
            for(int i=0;'\0'!=str[i];i++) cnt++;
            for(int i=0;i<(15-cnt);i++) printf(" ");
            for(int i=0;i<cnt;i++) printf("%c",str[i]);
            char timestamp[20];
            sprintf(timestamp," %s",ctime(&info.st_mtime));
            for(int i=4;i<17;i++) printf("%c",timestamp[i]); 
            // char time_stamp[50];
            // strftime(time_stamp,50,"%m  %d ",localtime(&info.st_mtime));
            // printf(" %s",time_stamp);
            printf(" %s\n",D->d_name);
            }
        }
        
    }
    //if(D->d_name[0]!='.')printf("%s\n",D->d_name);
    closedir(dr);
    chdir(namef);
    printf("\n");}
}
void ls_la(char*dir,char * tilde,int *bonus2){
    int errflag=0;
     char namef[1024];
     char name[1024];
    getcwd(namef,1024);
    int dir_siz=0;
    for(int i=0;dir[i]!='\0';i++) dir_siz++;
    if(dir_siz==1&&dir[0]=='~') {if(chdir(tilde)<0) {perror("error while using chdir in ls.c");(*bonus2)*=0;}
    else errflag=1;}
    else if(dir_siz>=2&&dir[0]=='~'&&dir[1]=='/') {
        char ex[1024];
        int k=0,x=0;
        for(int i=0;tilde[i]!='\0';i++) ex[k++]=tilde[i];
        for(int i=1;i<dir_siz;i++) ex[k++]=dir[i];
        for(int i=0;i<k;i++) dir[x++]=ex[i];
        dir[x]='\0';
        //printf("%s\n",dir);
        if(chdir(dir)<0) {perror("error while using chdir in ls.c");(*bonus2)*=0;}
        else errflag=1;
        //pwd();
    } 
    else {if(chdir(dir)<0) {perror("error while using chdir in ls.c");(*bonus2)*=0;}
    else errflag=1;}
    if(errflag==1){
    getcwd(name,1024);
    struct dirent *D;
    struct stat info;
    struct passwd *u;
    struct group *g;
    DIR *dr = opendir(name);
    if(dr== NULL) perror("error while using opendir");
    while((D=readdir(dr))!=NULL) {
        if(stat(D->d_name,&info)<0) perror("error while using stat");
        else{
            if(1){
            //printf("total: %d\n",info.st_blk_size)
            //ans+=info.st_blksize;
            printf((info.st_mode & S_IFDIR)?"d":"-");  
            printf((info.st_mode & S_IRUSR)?"r":"-");
            printf((info.st_mode & S_IWUSR)?"w":"-");
            printf((info.st_mode & S_IXUSR)?"x":"-");
            printf((info.st_mode & S_IRGRP)?"r":"-");
            printf((info.st_mode & S_IWGRP)?"w":"-");
            printf((info.st_mode & S_IXGRP)?"x":"-");
            printf((info.st_mode & S_IROTH)?"r":"-");
            printf((info.st_mode & S_IWOTH)?"w":"-");
            printf((info.st_mode & S_IXOTH)?"x":"-");
            printf(" %10lu",info.st_nlink);
            u=getpwuid(info.st_uid);
            g=getgrgid(info.st_gid);
            printf(" %20s %20s",u->pw_name,g->gr_name);
            char str[100];
            sprintf(str," %ld",info.st_size);
            int cnt=0;
            for(int i=0;str[i]!='\0';i++) cnt++;
            for(int i=0;i<(15-cnt);i++) printf(" ");
            for(int i=0;i<cnt;i++) printf("%c",str[i]);
            char timestamp[20];
            sprintf(timestamp," %s",ctime(&info.st_mtime));
            for(int i=4;i<17;i++) printf("%c",timestamp[i]); 
            printf(" %s\n",D->d_name);
            }
        }
        
    }
    closedir(dr);
    chdir(namef);
    printf("\n");
    }
}