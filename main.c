#include "headers.h"
#include "prompt.h"
#include "pwd.h"
#include "echo.h"
#include "cd.h"
#include "beautify.h"
#include "ls.h"
#include "pinfo.h"
#include "env.h"
int cnt;
int bonus2=-1;
struct node{
    char name[1024];
    int id;
    int cnt;
    struct node * next;
};
struct node *head ;
void display(){
    struct node * fake=(struct node*)malloc(sizeof(struct node));
    fake=head;
    while(fake){
        printf("%d\t",fake->id);
        fake=fake->next;
    }
    return;
}
int f_id=-1;
char *f_name=NULL;
void ctrlc(){
    if(f_id==-1){
        (bonus2)*=0;
        return;
    } 
    if(kill(f_id,9)<0){
        //perror("error while using kill in ctrlc");
        bonus2*=0;
        return;
    }
    bonus2*=0;
    return;
}
void ctrlz(){
    if(f_id==-1){
        (bonus2)*=0;
        return;
    }
    if(kill(f_id,19)<0){
        //perror("error while using kill in ctrlz.");
        bonus2*=0;
        return;
    }
    struct node *fake=(struct node *)malloc(sizeof(struct node));
    struct node *list=(struct node *)malloc(sizeof(struct node));
    fake=head;
    while(fake->next){
        fake=fake->next;
    }
    list->id=f_id;
    list->cnt=cnt++;
    for(int i=0;f_name[i]!='\0';i++){
        list->name[i]=f_name[i];
    }
    list->next=NULL;
    fake->next=list;
    bonus2*=0;
    return;
}
void termination(){
    struct node * fake=(struct node *)malloc(sizeof(struct node));
    struct node * prev=(struct node *)malloc(sizeof(struct node));
    prev=head;fake=head;
    while(fake!=NULL){
        int status;
        if(fake->id==-1){
            fake=fake->next;
            continue;
        }
        if((waitpid(fake->id,&status,WNOHANG))>0){
            if(WIFEXITED(status)) printf("%s with pid %d exited normally",fake->name,fake->id);
            else printf("%s with pid %d exited abruptly",fake->name,fake->id);
            printf("\n");
            if(fake->next==NULL){
                prev->next=NULL;  
                break;  
            }
            else prev->next=fake->next;
        }
        else prev=fake;
        fake=fake->next;
    }
    return;
}
int spid;
int main()
{   

    signal(SIGINT,ctrlc);
    signal(SIGTSTP,ctrlz);
    cnt=0;
    head=(struct node *)malloc(sizeof(struct node));
    head->id=-1;
    head->name[0]='\0';
    head->cnt=cnt++;
    head->next=NULL;
    unsetenv("OLDPWD");
    struct node * present=(struct node *)malloc(sizeof(struct node));
    present=head;
    int sin=dup(0);
    int sout=dup(1);
    int sid = spid = getpid();
    char tilde[1024];
    char* dupl = (char*)calloc(1024,sizeof(char));
    int inp_siz=0,home_siz=0,inp1_siz;
    if(getcwd(tilde,1024)==NULL) {
        perror("error while using getcwd() in main.c");
        bonus2*=0;
    }
    for(int i=0;tilde[i]!='\0';i++) home_siz++;
    while(1){
        prompt(tilde,home_siz,&bonus2);
        bonus2=1;
        //dupl=inp(&head);
        size_t sizee=1024;
        if(getline(&dupl,&sizee,stdin)==-1){
             struct node * fake = (struct node *)malloc(sizeof(struct node));
            fake=head;
            while(fake){
                if(fake->id==-1){
                   fake=fake->next;
                   continue;
                }
                if(kill(fake->id,9)<0){
                    perror ("error while using kill in main.c. ");
                    bonus2*=0;
                }
                fake=fake->next;
            }
            exit(0); 
        }
        int dup_siz=0;
        for(int i=0;dupl[i]!='\0';i++) dup_siz++;
        dupl[dup_siz-1]='\0';
        int var=0;
        while(var<(dup_siz-1)){
            bonus2=1;
            char* input1 = (char*)calloc(1024,sizeof(char));
            inp1_siz=0;
            for(;(var<(dup_siz-1))&&(dupl[var]!=';');var++) input1[inp1_siz++]=dupl[var];
            //printf("%d ",inp1_siz);
            input1[inp1_siz]='\0';
            int var2=0;
            int fildes[2];
            int last=0,dupfd=dup(0);
            while(var2<(inp1_siz)){
                bonus2=1;
                char* input = (char*)calloc(1024,sizeof(char));
                inp_siz=0;
                if(pipe(fildes)<0){
                    perror("Couldn't create pipe");
                    bonus2*=0;
                    continue;
                }
                char input2[1024];
                int inp2_siz=0;
                for(;(var2<(inp1_siz))&&(input1[var2]!='|');var2++) input2[inp2_siz++]=input1[var2];
                if(var2>=inp1_siz) last=1;
                input2[inp2_siz]='\0';
                input=beautify(input2);
                dup2(dupfd,STDIN_FILENO);
                if(last==0) dup2(fildes[1],STDOUT_FILENO);
                else {
                    fflush(stdout);
                    dup2(sout,STDOUT_FILENO);
                }
                dup2(fildes[0],dupfd);
                close(fildes[0]);
                close(fildes[1]);
                char outfile[1024],infile[1024];
                int pj=0,in=0,out=0,append=0;
                for(int i=0;input[i]!='\0';i++) inp_siz++;
                for(int i=0;input[i]!='\0';i++){
                    if(input[i]=='>'&&input[i+1]=='>'){
                        i+=3;
                        append=1;
                        for(;input[i]!='\0'&&input[i]!=' ';i++){
                            outfile[pj++]=input[i];
                        }
                    }
                    else if(append==1){
                        input[i-pj-4]=input[i];
                    }
                }
                if(append!=1){
                for(int i=0;input[i]!='\0';i++){
                    if(input[i]=='>'){
                        i+=2;
                        out=1;
                        for(;input[i]!='\0'&&input[i]!=' ';i++){
                            outfile[pj++]=input[i];
                        }
                    }
                    else if(out==1){
                        input[i-pj-3]=input[i];
                    }
                }
                }
                int ij=0;
                for(int i=0;input[i]!='\0';i++){
                    if(input[i]=='<'){
                        i+=2;
                        in=1;
                        for(;input[i]!='\0'&&input[i]!=' ';i++){
                            infile[ij++]=input[i];
                        }
                    }
                    else if(in==1){
                        input[i-pj-3]=input[i];
                    }
                }
                outfile[pj]='\0';
                infile[ij]='\0';
                int fdt=dup(1);
                int fdi=dup(0);
                if(in==1){
                    inp_siz-=(ij+3);
                    int fd_out=open(infile,O_RDONLY);
                    //printf("%d",fd_out);
                    if(fd_out<0){
                     perror("error in opening input file");
                     bonus2*=0;
                     continue;
                    }
                    else dup2(fd_out,STDIN_FILENO);
                }
                if(append==1){
                    inp_siz-=(pj+4);
                    int fd_out=open(outfile,O_RDWR | O_CREAT,0644);
                    if(fd_out<0) {perror("error in opening output file");bonus2*=0;}
                    else dup2(fd_out,STDOUT_FILENO);
                    lseek(fd_out,0,SEEK_END);
                }
                if(out==1){
                    inp_siz-=(pj+3);
                    int fd_out=open(outfile,O_RDWR | O_CREAT | O_TRUNC,0644);
                    if(fd_out<0){ perror("error in opening output file");bonus2*=0;}
                    else dup2(fd_out,STDOUT_FILENO);
                }
                input[inp_siz]='\0';
                int back=0;
                if(input[inp_siz-1]=='&') back=1;
                if(inp_siz==3&&input[0]=='p'&&input[1]=='w'&&input[2]=='d') pwd();
                else if(inp_siz==4&&input[0]=='c'&&input[1]=='d'&&input[2]==' '&&input[3]=='-') {
                    char val[1024],path[1024];
                    char *oldpwd = (char*)calloc(1024,sizeof(char));
                    val[0]='O';
                    val[1]='L';
                    val[2]='D';
                    val[3]='P';
                    val[4]='W';
                    val[5]='D';
                    val[6]='\0';
                    if(!getcwd(path,1024)) {perror("error while using getcwd in main.c cd -\n");bonus2*=0;}
                    if(getenv(val)==NULL) {printf("No previous working directory.\n");bonus2*=0;}
                    else{
                        oldpwd=getenv(val);
                        printf("%s\n",oldpwd);
                        chdir(oldpwd);
                    }
                    setenv(val,path,1);
                }
                else if(inp_siz==4&&input[0]=='j'&&input[1]=='o'&&input[2]=='b'&&input[3]=='s'){
                    struct node * fake=(struct node *)malloc(sizeof(struct node));
                    fake=head;
                    char pathp[1024];
                    if(!getcwd(pathp,1024)) {perror("error while using getcwd in main.c cd -\n");bonus2*=0;}
                    while(fake!=NULL){
                        if(fake->id==-1){
                            fake=fake->next;
                            continue;
                        }
                        int id=fake->id;
                        char idchar[20];
                        int k=0;
                        while(id>0){
                            idchar[k++]=id%10+'0';
                            id/=10;
                        }
                        char path[1024];
                        path[0]='/';
                        path[1]='p';
                        path[2]='r';
                        path[3]='o';
                        path[4]='c';
                        path[5]='/';
                        for(int i=0;i<k;i++){
                            path[5+i+1]=idchar[k-i-1];
                        }
                        path[k+6]='\0';
                        chdir(path);
                        char arr[100000];
                        int fd=open("stat",O_RDONLY);
                        read(fd,arr,100000);
                        int flag=0;
                        int i=0;
                        while(flag!=2){
                            while(arr[i]!=' ') i++;
                            flag++;
                            i++;
                        }
                        char ch = arr[i];
                        if(ch=='R'||ch=='S') printf("[%d] Running %s [%d]\n",fake->cnt,fake->name,fake->id);
                        else printf("[%d] Stopped %s [%d]\n",fake->cnt,fake->name,fake->id);
                        fake=fake->next;
                        chdir(pathp);
                    }
                    
                }
                else if(inp_siz>5&&input[0]=='k'&&input[1]=='j'&&input[2]=='o'&&input[3]=='b'&&input[4]==' '){
                    int jid=0;
                    int sigid=0;
                    char j[1024];
                    char s[1024];
                    int k=5;
                    int flag=0;
                    while(input[k]!='\0'){
                        if(input[k]==' ') flag++;
                        k++;
                    }
                    if(flag!=1) {printf("ERROR : invalid number of arguments.\n");bonus2*=0;}
                    else{
                        k=5;
                        int fj=0;
                        int fs=0;
                        int bol=0;
                        while(input[k]!='\0'){
                            if(input[k]==' '){ bol=1;j[fj]='\0';}
                            else if(bol==0) j[fj++]=input[k];
                            else s[fs++]=input[k];
                            k++;
                        }
                        s[fs]='\0';
                        for(int i=0;i<fj;i++){
                            jid+=(j[i]-'0')*modex(10,fj-i-1,1000000007);
                        }
                        for(int i=0;i<fs;i++){
                            sigid+=(s[i]-'0')*modex(10,fs-i-1,1000000007);
                        }
                        struct node * fake=(struct node *)malloc(sizeof(struct node));
                        fake=head;
                        int flag=0;
                        while(fake){
                            if(fake->id==-1){
                                fake=fake->next;
                                continue;
                            }
                            if(jid==fake->cnt){
                                if(kill(fake->id,sigid)<0) {
                                    //printf("%d %d %s,%s\n",jid,sigid,j,s);
                                perror("error while using kill in main.c");
                                bonus2*=0;
                                }
                                flag=1;
                                break;
                            }
                            fake=fake->next;
                        }
                        if(flag==0) {printf("INVALID JOB NUMBER.\n");bonus2*=0;}
                    } 
                }
                else if(inp_siz==8&&input[0]=='o'&&input[1]=='v'&&input[2]=='e'&&input[3]=='r'&&input[4]=='k'&&input[5]=='i'&&input[6]=='l'&&input[7]=='l'){
                    struct node * fake=(struct node *)malloc(sizeof(struct node));
                    fake=head;
                    while(fake){
                        if(fake->id==-1){
                            fake=fake->next;
                            continue;
                        }
                        if(kill(fake->id,9)<0) {
                            perror("error while using kill in main.c");
                            bonus2*=0;
                        }
                        sleep(0.5);
                        fake=fake->next;
                    }
                }
                else if(inp_siz>3&&input[0]=='f'&&input[1]=='g'&&input[2]==' '){
                    char j[1024];
                    int i=3;
                    int flag=0;
                    while(input[i]!='\0'){
                        if(input[i]==' ') flag++;
                        i++;
                    }
                    if(flag!=0) {printf("ERROR : INVALID NUMBER OF ARGUMENTS.\n");bonus2*=0;}
                    else{
                        i=3;
                        int jid=0;
                        int k=0;
                        while(input[i]!='\0'){
                            j[k++]=input[i++];
                        }
                        for(int i=0;i<k;i++){
                            jid+=(j[i]-'0')*modex(10,k-1-i,1000000007);
                        }
                        struct node * fake=(struct node *)malloc(sizeof(struct node));
                        struct node * prev=(struct node *)malloc(sizeof(struct node));
                        prev=fake=head;
                        while(fake){
                            if(fake->id==-1){
                                fake=fake->next;
                                continue;
                            }
                            if(fake->cnt==jid){
                                int gid=getpgid(fake->id),status;
                                if(kill(fake->id,SIGCONT)<0) {printf("error while using kill in main.c.\n");bonus2*=0;}
                                signal(SIGTTOU,SIG_IGN);
                                tcsetpgrp(sin,gid);
                                signal(SIGTTOU,SIG_DFL);
                                waitpid(fake->id,&status,WUNTRACED);
                                signal(SIGTTOU,SIG_IGN);
                                tcsetpgrp(sin,getpgid(spid));
                                signal(SIGTTOU,SIG_DFL);
                                if(kill(spid,SIGCONT)<0) {printf("error while using kill in main.c.\n");bonus2*=0;}
                                prev->next=fake->next;
                                flag=1;
                                break;
                            }
                            prev=fake;
                            fake=fake->next;
                        }
                        if(flag==0){printf("INVALID JOB ID\n");bonus2*=0;}
                    }
                }
                else if(inp_siz>3&&input[0]=='b'&&input[1]=='g'&&input[2]==' '){
                    char j[1024];
                    int i=3;
                    int flag=0;
                    while(input[i]!='\0'){
                        if(input[i]==' ') flag++;
                        i++;
                    }
                    if(flag!=0) {printf("ERROR : INVALID NUMBER OF ARGUMENTS.\n");bonus2*=0;}
                    else{
                        i=3;
                        int jid=0;
                        int k=0;
                        while(input[i]!='\0'){
                            j[k++]=input[i++];
                        }
                        for(int i=0;i<k;i++){
                            jid+=(j[i]-'0')*modex(10,k-1-i,1000000007);
                        }
                        struct node * fake=(struct node *)malloc(sizeof(struct node));
                        fake=head;
                        while(fake){
                            if(fake->id==-1){
                                fake=fake->next;
                                continue;
                            }
                            if(fake->cnt==jid){
                                if(kill(fake->id,SIGCONT)<0) {perror("error while using kill in main.c.");bonus2*=0;}
                                flag=1;
                                break;
                            }
                            fake=fake->next;
                        }
                        if(flag==0) {printf("INVALID JOB ID\n");bonus2*=0;}
                    }
                }
                else if(inp_siz==4&&input[0]=='q'&&input[1]=='u'&&input[2]=='i'&&input[3]=='t') {
                     struct node * fake=(struct node *)malloc(sizeof(struct node));
                    fake=head;
                    while(fake){
                        if(fake->id==-1){
                            fake=fake->next;
                            continue;
                        }
                        if(kill(fake->id,9)<0) {
                            perror("error while using kill in main.c");
                        }
                        fake=fake->next;
                    }
                    exit(0);
                }
                
                else if(inp_siz>6&&input[0]=='s'&&input[1]=='e'&&input[2]=='t'&&input[3]=='e'&&input[4]=='n'&&input[5]=='v') setenvi(input,&bonus2);
                else if(inp_siz==6&&input[0]=='s'&&input[1]=='e'&&input[2]=='t'&&input[3]=='e'&&input[4]=='n'&&input[5]=='v') {printf("ERROR :setenv without variable and value\n");bonus2*=0;}
                else if(inp_siz>=8&&input[0]=='u'&&input[1]=='n'&&input[2]=='s'&&input[3]=='e'&&input[4]=='t'&&input[5]=='e'&&input[6]=='n'&&input[7]=='v') unsetenvi(input,&bonus2);
                else if(inp_siz>3&&input[0]=='p'&&input[1]=='w'&&input[2]=='d'&&input[3]==' ') pwd();
                else if((inp_siz==4&&input[0]=='e'&&input[1]=='c'&&input[2]=='h'&&input[3]=='o')) printf("\n");
                else if((inp_siz>=5&&input[0]=='e'&&input[1]=='c'&&input[2]=='h'&&input[3]=='o'&&input[4]==' ')) echo(input,inp_siz);
                else if(inp_siz==2&&input[0]=='c'&&input[1]=='d'){
                    char path[1024];
                    int k=0;
                    for(int i=0;i<home_siz;i++) path[k++]=tilde[i];
                    char value[1024];
                    if(!getcwd(value,1024)) {perror("error while using getcwd in main.c cd -.\n");bonus2*=0;}
                    if(chdir(path)<0) {perror("error while using chdir.");bonus2*=0;}
                    else{
                        if(setenv("OLDPWD",value,1)<0){
                            printf("error while using setenv in main.c\n");
                            bonus2*=0;
                        }
                    }
                }
                else if((inp_siz==4&&input[0]=='c'&&input[1]=='d'&&input[2]==' '&&input[3]=='~')||(inp_siz>=5&&input[0]=='c'&&input[1]=='d'&&input[2]==' '&&input[3]=='~'&&input[4]=='/')){
                    char path[1024];
                    int k=0;
                    for(int i=0;i<home_siz;i++) path[k++]=tilde[i];
                    for(int i=4;i<inp_siz;i++) path[k++]=input[i];
                    char value[1024];
                    if(!getcwd(value,1024)){ perror("error while using getcwd in main.c cd -.\n");bonus2*=0;}
                    if(chdir(path)<0) {perror("error while using chdir.");bonus2*=0;}
                    else{
                        if(setenv("OLDPWD",value,1)<0){
                            printf("error while using setenv\n");
                            bonus2*=0;
                        }
                    }

                }
                else if(inp_siz>=3&&input[0]=='c'&&input[1]=='d'&&input[2]==' ') cd(input,inp_siz,&bonus2);
                else if((inp_siz==2&&input[0]=='l'&&input[1]=='s')) ls(".",tilde,&bonus2);
                else if(inp_siz==5&&input[0]=='l'&&input[1]=='s'&&input[2]==' '&&input[3]=='-'&&input[4]=='l') ls_l(".",tilde,&bonus2);
                else if(inp_siz==5&&input[0]=='l'&&input[1]=='s'&&input[2]==' '&&input[3]=='-'&&input[4]=='a') ls_a(".",tilde,&bonus2);
                else if(inp_siz==6&&input[0]=='l'&&input[1]=='s'&&input[2]==' '&&input[3]=='-'&&input[4]=='l'&&input[5]=='a') ls_la(".",tilde,&bonus2);
                else if(inp_siz==6&&input[0]=='l'&&input[1]=='s'&&input[2]==' '&&input[3]=='-'&&input[4]=='a'&&input[5]=='l') ls_la(".",tilde,&bonus2);
                else if(inp_siz==8&&input[0]=='l'&&input[1]=='s'&&input[2]==' '&&input[3]=='-'&&input[4]=='l'&&input[5]==' '&&input[6]=='-'&&input[7]=='a') ls_la(".",tilde,&bonus2);
                else if(inp_siz==8&&input[0]=='l'&&input[1]=='s'&&input[2]==' '&&input[3]=='-'&&input[4]=='a'&&input[5]==' '&&input[6]=='-'&&input[7]=='l') ls_la(".",tilde,&bonus2);
                else if(inp_siz>3&&input[0]=='l'&&input[1]=='s'&&input[2]==' '){
                    int l=0,a=0,error=0,nod=0;
                    char dir[1024][1024];
                    for(int i=3;i<inp_siz;i++){
                        if(input[i]=='-'){
                            i++;
                            while(i<inp_siz&&input[i]!=' '){
                                if(input[i]=='l') l=1;
                                else if(input[i]=='a') a=1;
                                i++;
                            }
                        }
                        else if(input[i]!=' '){
                            int dir_siz=0;
                            while(i<inp_siz&&input[i]!=' '){
                                dir[nod][dir_siz++]=input[i];
                                i++;
                            }
                            dir[nod][dir_siz]='\0';
                            nod++;
                        }
                        
                    }
                    if(nod==1) {
                        if(l==1&&a==1) ls_la(dir[0],tilde,&bonus2);
                        else if(l==0&&a==0) ls(dir[0],tilde,&bonus2);
                        else if(l==1&&a==0) ls_l(dir[0],tilde,&bonus2);
                        else ls_a(dir[0],tilde,&bonus2); 
                    }
                    else{
                    for(int i=0;i<nod;i++){
                        printf("%s :\n",dir[i]);
                        if(l==1&&a==1) ls_la(dir[i],tilde,&bonus2);
                        else if(l==0&&a==0) ls(dir[i],tilde,&bonus2);
                        else if(l==1&&a==0) ls_l(dir[i],tilde,&bonus2);
                        else ls_a(dir[i],tilde,&bonus2);  
                    }
                    }
                }
                else if(inp_siz==5&&input[0]=='p'&&input[1]=='i'&&input[2]=='n'&&input[3]=='f'&&input[4]=='o'){
                    pinfo(sid,tilde,home_siz);
                }
                else if(inp_siz>=6&&input[0]=='p'&&input[1]=='i'&&input[2]=='n'&&input[3]=='f'&&input[4]=='o'&&input[5]==' '){
                    int ans=0;
                    for(int i=inp_siz-1;i>=6;i--) ans+=(input[i]-'0')*modex(10,inp_siz-1-i,1000000000000000000);
                    pinfo(ans,tilde,home_siz);
                }
                else if(inp_siz==0) continue;
                else{
                    char str[1024];
                    int k=0;
                    for(int i=0;i<inp_siz;i++) str[k++]=input[i];
                    str[k]='\0';
                    if(back==1){
                    if(input[inp_siz-2]==' ') input[inp_siz-2]='\0';
                    else input[inp_siz-1]='\0';}
                    char *part[1024];
                    parse(input,part);
                    int forkid;
                    if(back==0){
                        forkid=fork();
                        if(forkid==0){
                            if(execvp(part[0],part)<0) {
                                printf("error no such command.\n");
                                bonus2*=0;
                                return 0;
                            }
                        }
                        else{
                            f_id=forkid;
                            free(f_name);
                            int k=0;
                            f_name = (char*)calloc(1024,sizeof(char));
                            for(int i=0;input[i]!='\0';i++) f_name[k++]=input[i];
                            f_name[k]='\0';
                            int status;
                            waitpid(forkid,&status,WUNTRACED);
                        }
                    }
                    else{
                        forkid=fork();
                        if(forkid==0){
                            setpgid(0,0);
                            if(execvp(part[0],part)<0) {
                                printf("error no such command\n");
                                kill(getpid(),9); 
                                return 0;
                            }
                        }
                        else{
                            struct node * list=(struct node *)malloc(sizeof(struct node));
                            struct node * fake=(struct node *)malloc(sizeof(struct node));
                            fake=head;
                            list->id=forkid;
                            int k=0;
                            for(int i=0;input[i]!='\0';i++) list->name[k++]=input[i];
                            list->name[k]='\0';
                            list->cnt=cnt++;
                            list->next=NULL;
                            while(fake->next!=NULL){
                                fake=fake->next;
                            }
                            fake->next=list;
                            signal(SIGCHLD,termination);
                        }
                    }
                }
                dup2(fdi,STDIN_FILENO);
                fflush(stdout);
                dup2(fdt,STDOUT_FILENO);
                close(fdi);
                close(fdt);
                var2++;
            }
            close(dupfd);
            dup2(sin,STDIN_FILENO);
            fflush(stdout);
            dup2(sout,STDOUT_FILENO);
            var++;
        }
    }
    return 0;
}