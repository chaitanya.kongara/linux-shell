#include "headers.h"
#include "pinfo.h"

void pinfo(int id,char *tilde,int home_siz){
     ///printf(".%d",id);
     int var=id;
    char start[1024];
    char new[1024];
    char dig[200];
    char str[4000];
    int k=0;
    while(id>0){
        dig[k++]=((id%10)+'0');
        id=id/10;
    }
    dig[k]='\0';
    if(getcwd(start,1024)<0) {
        printf("error : in getting current working directory\n");
    }
    new[0]='/';
    new[1]='p';
    new[2]='r';
    new[3]='o';
    new[4]='c';
    new[5]='/';

    for(int i=k-1;i>=0;i--){
        new[5+k-i]=dig[i];
    }
    new[5+k+1]='\0';
    if(chdir(new)<0) {printf("error in going to /proc/pid directory");}
    else{
    new[5+k+1]='/';
    new[5+k+2]='e';
    new[5+k+3]='x';
    new[5+k+4]='e';
    //printf("%s\n",new);
    char pstat[1024];
    char vmem[1024];
    char path[10000];
    int fd,flag=0;
    fd = open("stat",O_RDONLY);
    if(fd<0) {perror("error in opening file");return;}
    else{
    int cnt;
    lseek(fd , 0 , SEEK_SET );
    read(fd , str, 4000);
    //printf("%s\n",str);
    cnt=0;
    for(int i=0;str[i]!='\0';i++) cnt++;
    for(int i=0;i<cnt&&flag<=22;i++){
        k=0;
        while(flag==2&&str[i]!=' ') pstat[k++]=str[i++];
        if(flag==2) pstat[k]='\0';
        while(flag==22&&str[i]!=' ') vmem[k++]=str[i++];
        if(flag==22) vmem[k]='\0';
        while(flag!=2&&flag!=22&&str[i]!=' ') i++;
        flag++;
    }
    readlink(new,path,10000);
    printf("pid -- %d\n",var);
    printf("Process Status -- %s\n",pstat);
    printf("memory -- %s {Virtual Memory}\n",vmem);
    int i=0;
    for(i=0;path[i]!='\0';i++);
    int path_siz=i;
    char chai[1024];
    flag=0;
    k=0;
    if(path_siz>=home_siz){
        for(int i=0;i<home_siz;i++){
                if(path[i]!=tilde[i]){
                    flag=1;
                    break;
                }
            }
            if(flag==0){
                chai[k++]='~';
                for(int i=home_siz;i<path_siz;i++) chai[k++]=path[i];
                new[k]='\0';
                printf("Executable Path -- %s\n",chai);
            }
            else{
                if(i!=0)printf("Executable Path -- %s\n",path);
                else printf("Executable Path -- ERROR\n");
            }
    }
    else{
        if(i!=0) printf("Executable Path -- %s\n",path);
        else printf("Executable Path -- ERROR\n");
    }}
    chdir(start);
    for(int i=0;i<10000;i++) path[i]='\0';}
}




long long modex(long long a, long long b, long long mod){
	long long ans=1;
	while(b){
		if(b%2) ans=(ans*a)%mod;
		a=(a*a)%mod;
		b/=2;
	}
	return ans;
}
